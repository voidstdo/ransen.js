# ranSen.js ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

ranSen.js is a simple randomizer for user-defined sentences


## Usage
Open **ransen.js** and change the Sentenses inside the array:
```js
var senG = [
    {
        senS: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
        senSID: 1
    },
    {
        senS: 'Temporibus eaque labore quidem! Quos quidem repellat quis a cumque',
        senSID: 2
    },
    {
        senS: 'Laudantium id amet, magnam laborum! Error aperiam iste eos rem eveniet eius',
        senSID: 3
    },
    {
        senS: 'Consectetur adipisicing elit. Expedita et consectetur Lorem ipsum dolor sit amet, dolorum',
        senSID: 4
    }
];

```
**To start the script:**

```js
function randomizeSentenses() {
	ranD = senG.shuffle(); // This will shuffle the array senG

    for (var i = 0; i < ranD.length; i++) {
		console.log(ranD[i].senS); // Do what you want with the sentenses
    }
}

```


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)