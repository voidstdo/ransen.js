/*
	ranSen.js v1.05
	Random Sentences

	@Created By: Void Studio
	@Website: http://www.voidstdo.com
	@Contact: eduardo.dutra@voidstdo.com
	@Git Repository: https://gitlab.com/voidstdo/ransen.js

	@Licensed under MIT ( https://choosealicense.com/licenses/mit/ )

	Last Updated: September, 2018
*/


Array.prototype.shuffle = function () {
    for (var i = this.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = this[i];
        this[i] = this[j];
        this[j] = temp;
    }
    return this;
}

var senG = [
    {
        senS: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit',
        senSID: 1
    },
    {
        senS: 'Temporibus eaque labore quidem! Quos quidem repellat quis a cumque',
        senSID: 2
    },
    {
        senS: 'Laudantium id amet, magnam laborum! Error aperiam iste eos rem eveniet eius',
        senSID: 3
    },
    {
        senS: 'Consectetur adipisicing elit. Expedita et consectetur Lorem ipsum dolor sit amet, dolorum',
        senSID: 4
    }
];